<?php

/**
 * Fattura24.com
 *
 * @author    Fattura24.com
 * @copyright Fattura24.com
 * @license   You are just allowed to modify this copy for your own use. You must not redistribute it. License
 *               is permitted for one Prestashop instance only but you can install it on your test instances.
 */
    
class Fattura24 extends Module
{
    public function __construct()
    {
        $this->name = 'fattura24';
        $this->tab = 'administration';
        //[30.08.2018]
        // Gaetano
        //IMPORTANTE da modificare insieme al config_it.xml 
        $this->version = '1.7.26';
        $this->moduleDate = '2018/04/04 15:00';
        $this->author = 'Fattura24.com';
        $this->displayName = $this->l('Fattura24.com');
        $this->description = $this->l('Crea le tue fatture con Fattura24.com');
        $this->module_key = '00cd0339f7de777834493205747ac875';
        $this->need_instance = 0;
        $this->bootstrap = true;
        parent::__construct();
    }

    public function install()
    {
        // set default configuration values on first installation
        if(strlen(Configuration::get('PS_FATTURA24_API')) == 0)
            Configuration::updateValue('PS_FATTURA24_API', '');
        if(strlen(Configuration::get('PS_SALVA_CLIENTE')) == 0)
            Configuration::updateValue('PS_SALVA_CLIENTE', '1');
        if(strlen(Configuration::get('PS_CREAZIONE_ORDINE')) == 0)
            Configuration::updateValue('PS_CREAZIONE_ORDINE', '1');
        if(strlen(Configuration::get('PS_PDF_ORDINE')) == 0)
            Configuration::updateValue('PS_PDF_ORDINE', '0');
        if(strlen(Configuration::get('PS_MAIL_ORDINE')) == 0)
            Configuration::updateValue('PS_MAIL_ORDINE', '0');
        if(strlen(Configuration::get('PS_F24_MAGAZZINO_ORDINE')) == 0)
            Configuration::updateValue('PS_F24_MAGAZZINO_ORDINE', '0');
        if(strlen(Configuration::get('PS_F24_MODELLO_ORDINE')) == 0)
            Configuration::updateValue('PS_F24_MODELLO_ORDINE', 'Predefinito');
        if(strlen(Configuration::get('PS_F24_MODELLO_ORDINE_DEST')) == 0)
            Configuration::updateValue('PS_F24_MODELLO_ORDINE_DEST', 'Predefinito');
        if(strlen(Configuration::get('PS_CREAZIONE_FATTURA')) == 0)
            Configuration::updateValue('PS_CREAZIONE_FATTURA', 'Disattivata');
        if(strlen(Configuration::get('PS_EMAIL_FATTURA')) == 0)
            Configuration::updateValue('PS_EMAIL_FATTURA', '0');
        if(strlen(Configuration::get('PS_F24_MAGAZZINO_FATTURA')) == 0)
            Configuration::updateValue('PS_F24_MAGAZZINO_FATTURA', '0');
        if(strlen(Configuration::get('PS_STATO_PAGATO')) == 0)
            Configuration::updateValue('PS_STATO_PAGATO', '0');
        if(strlen(Configuration::get('PS_DISABILITA_RICEVUTE')) == 0)
            Configuration::updateValue('PS_DISABILITA_RICEVUTE', '0');
        if(strlen(Configuration::get('PS_F24_MODELLO_FATTURA')) == 0)
            Configuration::updateValue('PS_F24_MODELLO_FATTURA', 'Predefinito');
        if(strlen(Configuration::get('PS_F24_MODELLO_FATTURA_DEST')) == 0)
            Configuration::updateValue('PS_F24_MODELLO_FATTURA_DEST', 'Predefinito');
        if(strlen(Configuration::get('PS_F24_PDC')) == 0)
            Configuration::updateValue('PS_F24_PDC', 'Nessun Pdc');
        if(strlen(Configuration::get('PS_F24_SEZIONALE_RICEVUTA')) == 0)
            Configuration::updateValue('PS_F24_SEZIONALE_RICEVUTA', 'Predefinito');
        if(strlen(Configuration::get('PS_F24_SEZIONALE_FATTURA')) == 0)
            Configuration::updateValue('PS_F24_SEZIONALE_FATTURA', 'Predefinito');
        
        $dim = "UPDATE `" . _DB_PREFIX_ . "order_state` SET `pdf_invoice` = '0' WHERE `id_order_state` = '2'";
        Db::getInstance()->execute($dim);

        if (!parent::install())
            return false;
        
        // add docIdFattura24 column to Order Table (Invoice ID in Fattura24)
        try
        {
            $sql = 'SELECT docIdFattura24 from '._DB_PREFIX_.'orders';
            $result = Db::getInstance()->ExecuteS($sql);
            if(!$result)
                Db::getInstance()->execute('ALTER TABLE '._DB_PREFIX_.'orders ADD docIdFattura24 varchar(255) DEFAULT NULL');
        }
        catch(Exception $e)
        {
            Db::getInstance()->execute('ALTER TABLE '._DB_PREFIX_.'orders ADD docIdFattura24 varchar(255) DEFAULT NULL');
        }
        
        // add docIdOrderFattura24 column to Order Table (Order ID in Fattura24)
        try
        {
            $sql = 'SELECT docIdOrderFattura24 from '._DB_PREFIX_.'orders';
            $result = Db::getInstance()->ExecuteS($sql);
            if(!$result)
                Db::getInstance()->execute('ALTER TABLE '._DB_PREFIX_.'orders ADD docIdOrderFattura24 varchar(255) DEFAULT NULL');
        }
        catch(Exception $e)
        {
            Db::getInstance()->execute('ALTER TABLE '._DB_PREFIX_.'orders ADD docIdOrderFattura24 varchar(255) DEFAULT NULL');
        }
        
        // register hooks      
        if(!$this->registerHook('actionValidateOrder'))
            $this->registerHook('actionValidateOrder');
        if(!$this->registerHook('actionOrderStatusUpdate'))
            $this->registerHook('actionOrderStatusUpdate');  
        if(!$this->registerHook('actionOrderStatusPostUpdate'))
            $this->registerHook('actionOrderStatusPostUpdate');

        // create pdf folder
        $documentsFolder = _PS_DOWNLOAD_DIR_ . 'Fattura24/';
        if (!file_exists($documentsFolder))
            mkdir($documentsFolder , 0777, true);
        if (!file_exists($documentsFolder . 'index.php'))
            file_put_contents($documentsFolder . 'index.php', '<?php');
        
        $this->trace('Modulo installato correttamente!');
        
        return true;
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    public function getContent()
    {
        $output = null;
        if (Tools::isSubmit('submitModule'))
        {
            Configuration::updateValue('PS_FATTURA24_API', $_REQUEST['PS_FATTURA24_API']);

            //se le checkbox sono settate imposto il valore della relativa configurazione a 1, altrimenti a 0
            if (isset($_REQUEST['PS_SALVA_CLIENTE_']))
            	Configuration::updateValue('PS_SALVA_CLIENTE', $_REQUEST['PS_SALVA_CLIENTE_']);
            else
                Configuration::updateValue('PS_SALVA_CLIENTE', '0');
            
            if (isset($_REQUEST['PS_CREAZIONE_ORDINE_']))
                Configuration::updateValue('PS_CREAZIONE_ORDINE', $_REQUEST['PS_CREAZIONE_ORDINE_']);
            else
                Configuration::updateValue('PS_CREAZIONE_ORDINE', '0');

            if (isset($_REQUEST['PS_PDF_ORDINE_']))
                Configuration::updateValue('PS_PDF_ORDINE', $_REQUEST['PS_PDF_ORDINE_']);
            else
                Configuration::updateValue('PS_PDF_ORDINE', '0');

            if (isset($_REQUEST['PS_MAIL_ORDINE_']))
                Configuration::updateValue('PS_MAIL_ORDINE', $_REQUEST['PS_MAIL_ORDINE_']);
            else
                Configuration::updateValue('PS_MAIL_ORDINE', '0');

            if (isset($_REQUEST['PS_F24_MODELLO_ORDINE_']))
                Configuration::updateValue('PS_F24_MODELLO_ORDINE', $_REQUEST['PS_F24_MODELLO_ORDINE_']);
            else
                Configuration::updateValue('PS_F24_MODELLO_ORDINE', 'Predefinito');

            if (isset($_REQUEST['PS_F24_MODELLO_ORDINE_DEST_']))
                Configuration::updateValue('PS_F24_MODELLO_ORDINE_DEST', $_REQUEST['PS_F24_MODELLO_ORDINE_DEST_']);
            else
                Configuration::updateValue('PS_F24_MODELLO_ORDINE_DEST', 'Predefinito');

            if (isset($_REQUEST['PS_CREAZIONE_FATTURA_']))
                Configuration::updateValue('PS_CREAZIONE_FATTURA', $_REQUEST['PS_CREAZIONE_FATTURA_']);
            else
            Configuration::updateValue('PS_CREAZIONE_FATTURA', '0');

            if (isset($_REQUEST['PS_EMAIL_FATTURA_']))
                Configuration::updateValue('PS_EMAIL_FATTURA', $_REQUEST['PS_EMAIL_FATTURA_']);
            else
                Configuration::updateValue('PS_EMAIL_FATTURA', '0');

            if (isset($_REQUEST['PS_STATO_PAGATO_']))
                Configuration::updateValue('PS_STATO_PAGATO', $_REQUEST['PS_STATO_PAGATO_']);
            else
                Configuration::updateValue('PS_STATO_PAGATO', '0');
            
            if (isset($_REQUEST['PS_DISABILITA_RICEVUTE_']))
            	Configuration::updateValue('PS_DISABILITA_RICEVUTE', $_REQUEST['PS_DISABILITA_RICEVUTE_']);
            else
            	Configuration::updateValue('PS_DISABILITA_RICEVUTE', '0');

            if (isset($_REQUEST['PS_F24_MAGAZZINO_ORDINE_']))
            	Configuration::updateValue('PS_F24_MAGAZZINO_ORDINE', $_REQUEST['PS_F24_MAGAZZINO_ORDINE_']);
            else
            	Configuration::updateValue('PS_F24_MAGAZZINO_ORDINE', '0');

            if (isset($_REQUEST['PS_F24_MAGAZZINO_FATTURA_']))
            	Configuration::updateValue('PS_F24_MAGAZZINO_FATTURA', $_REQUEST['PS_F24_MAGAZZINO_FATTURA_']);
            else
                Configuration::updateValue('PS_F24_MAGAZZINO_FATTURA', '0');
            
            if (isset($_REQUEST['PS_F24_MODELLO_FATTURA_']))
                Configuration::updateValue('PS_F24_MODELLO_FATTURA', $_REQUEST['PS_F24_MODELLO_FATTURA_']);
            else
                Configuration::updateValue('PS_F24_MODELLO_FATTURA', 'Predefinito');
            
            if (isset($_REQUEST['PS_F24_MODELLO_FATTURA_DEST_']))
                Configuration::updateValue('PS_F24_MODELLO_FATTURA_DEST', $_REQUEST['PS_F24_MODELLO_FATTURA_DEST_']);
            else
                Configuration::updateValue('PS_F24_MODELLO_FATTURA_DEST', 'Predefinito');
            
            if (isset($_REQUEST['PS_F24_PDC_']))
                Configuration::updateValue('PS_F24_PDC', $_REQUEST['PS_F24_PDC_']);
            else
                Configuration::updateValue('PS_F24_PDC', 'Nessun Pdc');

            if (isset($_REQUEST['PS_F24_SEZIONALE_RICEVUTA_']))
                Configuration::updateValue('PS_F24_SEZIONALE_RICEVUTA', $_REQUEST['PS_F24_SEZIONALE_RICEVUTA_']);
            else
                Configuration::updateValue('PS_F24_SEZIONALE_RICEVUTA', 'Predefinito');

            if (isset($_REQUEST['PS_F24_SEZIONALE_FATTURA_']))
                Configuration::updateValue('PS_F24_SEZIONALE_FATTURA', $_REQUEST['PS_F24_SEZIONALE_FATTURA_']);
            else
                Configuration::updateValue('PS_F24_SEZIONALE_FATTURA', 'Predefinito');

            $output .= $this->displayConfirmation($this->l('Impostazioni aggiornate'));
        }

        $this->context->smarty->assign(
            array(
                'isFattura24Enabled' => Module::isEnabled('fattura24'),
                'psDisableOverrides' => Configuration::get('PS_DISABLE_OVERRIDES'),
                'version' => $this->version,
                'versionCheckMessage' => $this->getVersionCheckMessage(),
                'apiKey' => Configuration::get('PS_FATTURA24_API'),
                'apiKeyCheck' => $this->testApiKey(),
                'listaModelliOrdine' => $this->getTemplate(true),
                'modelloOrdine' => Configuration::get('PS_F24_MODELLO_ORDINE'),
                'modelloOrdineDest' => Configuration::get('PS_F24_MODELLO_ORDINE_DEST'),
                'listaModelliFattura' => $this->getTemplate(false),
                'opzioniCreaFattura' => ['Disattivata', 'Fattura elettronica', 'Fattura NON elettronica'],
                'modelloFattura' => Configuration::get('PS_F24_MODELLO_FATTURA'),
                'modelloFatturaDest' => Configuration::get('PS_F24_MODELLO_FATTURA_DEST'),
                'listaConti' => $this->getPdc(),
                'conto' => Configuration::get('PS_F24_PDC'),
                'salvaCliente' => Configuration::get('PS_SALVA_CLIENTE'),
                'creazioneOrdine' => Configuration::get('PS_CREAZIONE_ORDINE'),
                'pdfOrdine' => Configuration::get('PS_PDF_ORDINE'),
                'emailOrdine' => Configuration::get('PS_MAIL_ORDINE'),
                'creazioneFattura' => Configuration::get('PS_CREAZIONE_FATTURA'),
                'emailFattura' => Configuration::get('PS_EMAIL_FATTURA'),
                'statoPagato' => Configuration::get('PS_STATO_PAGATO'),
                'disabilitaRicevute' => Configuration::get('PS_DISABILITA_RICEVUTE'),
                'magazzinoOrdine' => Configuration::get('PS_F24_MAGAZZINO_ORDINE'),
                'magazzinoFattura' => Configuration::get('PS_F24_MAGAZZINO_FATTURA'),
                'listaSezionaliFattura' => $this->GetNumerator(1),
                'listaSezionaliRicevuta' => $this->GetNumerator(3),
                'sezionaleRicevuta' => Configuration::get('PS_F24_SEZIONALE_RICEVUTA'),
                'sezionaleFattura' => Configuration::get('PS_F24_SEZIONALE_FATTURA'),
                'moduleUrl' => _MODULE_DIR_ . 'fattura24/',
                'info' => $this->getInfo()
            )
        );

        $output .= $this->display(__FILE__, 'views/templates/admin/form.tpl');
        return $output;
    }

    public function getConfigFieldsValues()
    {
        return array(
            'PS_FATTURA24_API' => Tools::getValue('PS_FATTURA24_API', Configuration::get('PS_FATTURA24_API')),
        	'PS_SALVA_CLIENTE' => Tools::getValue('PS_SALVA_CLIENTE_', Configuration::get('PS_SALVA_CLIENTE')),
            'PS_CREAZIONE_ORDINE_' => Tools::getValue('PS_CREAZIONE_ORDINE_', Configuration::get('PS_CREAZIONE_ORDINE')),
            'PS_PDF_ORDINE_' => Tools::getValue('PS_PDF_ORDINE_', Configuration::get('PS_PDF_ORDINE')),
            'PS_MAIL_ORDINE_' => Tools::getValue('PS_MAIL_ORDINE_', Configuration::get('PS_MAIL_ORDINE')),
            'PS_F24_MODELLO_ORDINE_' => Tools::getValue('PS_F24_MODELLO_ORDINE_', Configuration::get('PS_F24_MODELLO_ORDINE')),
            'PS_F24_MODELLO_ORDINE_DEST_' => Tools::getValue('PS_F24_MODELLO_ORDINE_DEST_', Configuration::get('PS_F24_MODELLO_ORDINE_DEST')),
            'PS_CREAZIONE_FATTURA_' => Tools::getValue('PS_CREAZIONE_FATTURA_', Configuration::get('PS_CREAZIONE_FATTURA')),
            'PS_EMAIL_FATTURA_' => Tools::getValue('PS_EMAIL_FATTURA_', Configuration::get('PS_EMAIL_FATTURA')),
            'PS_STATO_PAGATO_' => Tools::getValue('PS_STATO_PAGATO_', Configuration::get('PS_STATO_PAGATO')),
            'PS_DISABILITA_RICEVUTE_' => Tools::getValue('PS_DISABILITA_RICEVUTE_', Configuration::get('PS_DISABILITA_RICEVUTE')),
            'PS_F24_MAGAZZINO_ORDINE_' => Tools::getValue('PS_F24_MAGAZZINO_ORDINE_', Configuration::get('PS_F24_MAGAZZINO_ORDINE')),
            'PS_F24_MAGAZZINO_FATTURA_' => Tools::getValue('PS_F24_MAGAZZINO_FATTURA_', Configuration::get('PS_F24_MAGAZZINO_FATTURA')),
            'PS_F24_MODELLO_FATTURA_' => Tools::getValue('PS_F24_MODELLO_FATTURA_', Configuration::get('PS_F24_MODELLO_FATTURA')),
            'PS_F24_MODELLO_FATTURA_DEST_' => Tools::getValue('PS_F24_MODELLO_FATTURA_DEST_', Configuration::get('PS_F24_MODELLO_FATTURA_DEST')),
            'PS_F24_PDC_' => Tools::getValue('PS_F24_PDC_', Configuration::get('PS_F24_PDC')),
            'PS_F24_SEZIONALE_FATTURA_' => Tools::getValue('PS_F24_SEZIONALE_FATTURA_', Configuration::get('PS_F24_SEZIONALE_FATTURA')),
            'PS_F24_SEZIONALE_RICEVUTA_' => Tools::getValue('PS_F24_SEZIONALE_RICEVUTA_', Configuration::get('PS_F24_SEZIONALE_RICEVUTA'))            
        );
    }

    // Hook per la creazione dell'ordine. Si attiva quando viene creato l'ordine
    public function hookActionValidateOrder($order)
    {
        if(!$this->active)
            return;
        // fix per compatibilità con Prestashop 1.7
        if(isset($order['order']))
            $check_order = $order['order'];
        else
            $check_order = $order['objOrder'];
    	if (Configuration::get('PS_CREAZIONE_ORDINE') == 1)
            $this->afterHook($check_order, false);
    	else if(Configuration::get('PS_SALVA_CLIENTE') == 1)
    		$this->saveCustomer($check_order);
    }
    
    // Hook per la creazione della fattura. Si attiva quando lo stato dell'ordine è impostato come pagato se l'ordine non ha una fattura creata tramite Prestashop
    public function hookActionOrderStatusUpdate($order) 
    {
        if(!$this->active || !$order['newOrderStatus']->paid || Configuration::get('PS_CREAZIONE_FATTURA') != 1 )
            return;
        $orderId = $order['id_order'];
        $check_order = new Order($orderId);
        if($check_order->hasInvoice())
            return;
        $query = 'SELECT docIdFattura24 FROM '._DB_PREFIX_.'orders WHERE id_order=\'' . $orderId . '\';';
        $docIdFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdFattura24'];
        if ($docIdFattura24 == NULL)
            $this->afterHook($check_order, true);
    }

    // Dato che voglio creare una fattura in Fattura24 solo se non è stata già creata in Prestashop, 
    // ho il problema che durante la creazione non conosco il numero della fattura per nominare il file nel modo corretto. 
    // Un modo di risolvere è usare questo hook per rinominare il file appena creato dall'hook hookActionOrderStatusUpdate con il nome corretto
    public function hookActionOrderStatusPostUpdate($order) {
        if(!$this->active || !$order['newOrderStatus']->paid || Configuration::get('PS_CREAZIONE_FATTURA') != 1 )
            return;
        $orderId = $order['id_order'];
        $check_order = new Order($orderId);
        $number = $check_order->invoice_number;
        if(empty($number))
            return;

        //[30.08.2018]
        // Gaetano
        //Qui costruisco il nome del file, contenente il progressivo fattura e l'anno che applicherò quando chiamo la funzione rename.
        $newFileName = $this->constructFileName($number,$check_order->date_add);
        
        //[30.08.2018]
        // Gaetano
        // Qui ricostruisco il nome del file pdf della fattura da rinominare
        // presente nella cartella di Download di Fattura24 contenuta nei file di Prestashop
        // con il progressivo fattura azzerato perchè quando viene creato, il plugin di Fattura24 non sa che numero assegnargli.
        $year = date("Y", strtotime($check_order->date_add));
        $oldFileName = 'FA000000-'. $year .'.pdf';
        
        //Qui viene fatta la rename del pdf
         $folder = _PS_DOWNLOAD_DIR_ . 'Fattura24/';
        rename($folder . $oldFileName, $folder . $newFileName);
    }

    // [30.08.2018]
    // Gaetano
    // Funzione generica per costruire o ricavare il nome della fattura pdf con il progressivo fattura e l'anno
    public function constructFileName($invoice_number, $date){
        $year = date("Y", strtotime($date));
        return 'FA' . sprintf('%06d', $invoice_number) . '-' . $year . '.pdf';
    }

    public function createCustomerXml($order_id)
    {
    	$domtree = new DOMDocument('1.0', 'UTF-8');
    	$domtree->preserveWhiteSpace = false;
    	$domtree->formatOutput = true;
    	$xmlRoot = $domtree->createElement("Fattura24");
    	$xmlRoot = $domtree->appendChild($xmlRoot);
    	$xmlCustomer = $domtree->createElement("Document");
    	$xmlCustomer = $xmlRoot->appendChild($xmlCustomer);
    	
    	$invoice_address = new Order($order_id);
    	$invoice_address = $invoice_address->id_address_invoice;
    	$ga = new Address($invoice_address);
        $customer = new Customer($ga->id_customer);
        
    	if(!empty($ga->company) && !empty($ga->vat_number))
    		$customername = $ga->company;
        else
    		$customername = $ga->firstname . ' ' . $ga->lastname;
        $customer_name = $domtree->createElement('CustomerName');
        $customer_name->appendChild($domtree->createCDataSection($customername));
        $xmlCustomer->appendChild($customer_name);
        $customer_address = $domtree->createElement('CustomerAddress');
        $customer_address->appendChild($domtree->createCDataSection($ga->address1 . ($ga->address2 == null ? "" : " " . $ga->address2)));
        $xmlCustomer->appendChild($customer_address);
    	$xmlCustomer->appendChild($domtree->createElement('CustomerPostcode', $ga->postcode));
    	$xmlCustomer->appendChild($domtree->createElement('CustomerCity', $ga->city));
        if (!empty($ga->id_state))
        {
    		$state = new State($ga->id_state);
    		$xmlCustomer->appendChild($domtree->createElement('CustomerProvince', $state->iso_code));
    	}
    	$fattura_invoice->appendChild($domtree->createElement('CustomerCountry', Country::getIsoById($ga->id_country)));
        if(!empty($ga->dni))
    	   $xmlCustomer->appendChild($domtree->createElement('CustomerFiscalCode', $ga->dni));
        if(!empty($ga->vat_number))
    	   $xmlCustomer->appendChild($domtree->createElement('CustomerVatCode', $ga->vat_number));
    	if(!empty($ga->phone))
    		$xmlCustomer->appendChild($domtree->createElement('CustomerCellPhone', $ga->phone));
    	else if (!empty($ga->phone_mobile))
    		$xmlCustomer->appendChild($domtree->createElement('CustomerCellPhone', $ga->phone_mobile));
        $customer_email = $domtree->createElement('CustomerEmail');
        $customer_email->appendChild($domtree->createCDataSection($customer->email));
        $xmlCustomer->appendChild($customer_email);
    	return $domtree->saveXML();
    }
    
    public function createInvoiceXml($order_id, $isInvoice)
    {
        $fixnum = function($n, $p)
        {
            return sprintf("%0.{$p}f", $n);
        };
        
        $domtree = new DOMDocument('1.0', 'UTF-8');
        $domtree->preserveWhiteSpace = false;
        $domtree->formatOutput = true;
        $xmlRoot = $domtree->createElement("Fattura24");
        $xmlRoot = $domtree->appendChild($xmlRoot);
        $fattura_invoice = $domtree->createElement("Document");
        $fattura_invoice = $xmlRoot->appendChild($fattura_invoice);

        $order = new Order($order_id);
        $this->trace('order details', $order);
        
        $invoice_address = $order->id_address_invoice;
        $ga = new Address($invoice_address);
        $customer = new Customer($ga->id_customer);

        if(!empty($ga->company) && !empty($ga->vat_number))
            $customername = $ga->company;
        else
            $customername = $ga->firstname . ' ' . $ga->lastname;
        
        $customer_name = $domtree->createElement('CustomerName');
        $customer_name->appendChild($domtree->createCDataSection($customername));
        $fattura_invoice->appendChild($customer_name);
        $customer_address = $domtree->createElement('CustomerAddress');
        $customer_address->appendChild($domtree->createCDataSection($ga->address1 . ($ga->address2 == null ? "" : " " . $ga->address2)));
        $fattura_invoice->appendChild($customer_address);
        $fattura_invoice->appendChild($domtree->createElement('CustomerPostcode', $ga->postcode));
        $customer_name = $domtree->createElement('CustomerCity');
        $customer_name->appendChild($domtree->createCDataSection($ga->city));
        $fattura_invoice->appendChild($customer_name);
        if (!empty($ga->id_state))
        {
            $state = new State($ga->id_state);
            $fattura_invoice->appendChild($domtree->createElement('CustomerProvince', $state->iso_code));
        }
        else
            $fattura_invoice->appendChild($domtree->createElement('CustomerProvince', ''));
        $fattura_invoice->appendChild($domtree->createElement('CustomerCountry', Country::getIsoById($ga->id_country)));
        if(!empty($ga->dni))
    	   $fattura_invoice->appendChild($domtree->createElement('CustomerFiscalCode', $ga->dni));
        if(!empty($ga->vat_number))
    	   $fattura_invoice->appendChild($domtree->createElement('CustomerVatCode', $ga->vat_number));
        if (!empty($ga->phone))
            $fattura_invoice->appendChild($domtree->createElement('CustomerCellPhone', $ga->phone));
        else if (!empty($ga->phone_mobile))
            $fattura_invoice->appendChild($domtree->createElement('CustomerCellPhone', $ga->phone_mobile));
        $customer_email = $domtree->createElement('CustomerEmail');
        $customer_email->appendChild($domtree->createCDataSection($customer->email));
        $fattura_invoice->appendChild($customer_email);
        
        if(!$order->isVirtual() && $order->id_address_delivery != null)
        {
            $delivery_address = new Address($order->id_address_delivery);            
            if(!empty($delivery_address->company))
                $customername = $delivery_address->company;
            else
                $customername = $delivery_address->firstname . ' ' . $delivery_address->lastname;
            
            $delivery_name = $domtree->createElement('DeliveryName');
            $delivery_name->appendChild($domtree->createCDataSection($customername));
            $fattura_invoice->appendChild($delivery_name);
            $address = $domtree->createElement('DeliveryAddress');
            $address->appendChild($domtree->createCDataSection($delivery_address->address1 . ($delivery_address->address2 == null ? "" : " " . $delivery_address->address2)));
            $fattura_invoice->appendChild($address);
            $fattura_invoice->appendChild($domtree->createElement('DeliveryPostcode', $delivery_address->postcode));
            $fattura_invoice->appendChild($domtree->createElement('DeliveryCity', $delivery_address->city));
            if (!empty($delivery_address->id_state))
            {
                $state = new State($ga->id_state);
                $fattura_invoice->appendChild($domtree->createElement('DeliveryProvince', $state->iso_code));
            }
            $fattura_invoice->appendChild($domtree->createElement('DeliveryCountry', Country::getIsoById($delivery_address->id_country)));
        }
        
        $fattura_invoice->appendChild($domtree->createElement('TotalWithoutTax', $order->total_paid_tax_excl));
        $payment_method_name = $domtree->createElement('PaymentMethodName');
        $orderPayment = $order->payment;
        if(!empty($orderPayment) && strpos(strtolower($orderPayment), 'paypal') !== false)
            $orderPayment = 'PayPal';
        $payment_method_name->appendChild($domtree->createCDataSection($orderPayment));
        $fattura_invoice->appendChild($payment_method_name);
        $payment_method_description = $domtree->createElement('PaymentMethodDescription');
        $payment_method_description->appendChild($domtree->createCDataSection($orderPayment));
        $fattura_invoice->appendChild($payment_method_description);
        $fattura_invoice->appendChild($domtree->createElement('VatAmount', ($order->total_paid_tax_incl) - ($order->total_paid_tax_excl)));
        $fattura_invoice->appendChild($domtree->createElement('Total', $order->total_paid));
        
        if ($isInvoice)
        {
            $payments = $fattura_invoice->appendChild($domtree->createElement('Payments'));
            $payment = $payments->appendChild($domtree->createElement('Payment'));
            $payment->appendChild($domtree->createElement('Date', $this->now('Y-m-d')));
            $payment->appendChild($domtree->createElement('Amount', $order->total_paid));
            if (Configuration::get('PS_STATO_PAGATO') == 1)
                $payment->appendChild($domtree->createElement('Paid', 'true'));
            else
                $payment->appendChild($domtree->createElement('Paid', 'false'));
        }
        
        $products = $fattura_invoice->appendChild($domtree->createElement('Rows'));
        $pdcIsConfigured=false;
        if(Configuration::get('PS_CREAZIONE_FATTURA') == 1)
        {
            $idPdc = Configuration::get('PS_F24_PDC');
            if(!empty($idPdc) && $idPdc != 'Nessun Pdc')
                $pdcIsConfigured=true;
        }
        foreach ($order->getProducts() as $product_detail)
        {
            $this->trace('item details', $product_detail);
            
            $product = $products->appendChild($domtree->createElement('Row'));
            $code = $domtree->createElement('Code');
            $code->appendChild($domtree->createCDataSection($product_detail['reference']));
            $product->appendChild($code);
            $description = $domtree->createElement('Description');
            $description->appendChild($domtree->createCDataSection($product_detail['product_name']));
            $product->appendChild($description);
            $product->appendChild($domtree->createElement('Qty', $product_detail['product_quantity']));
            $product->appendChild($domtree->createElement('Price', $product_detail['product_price']));
            $product->appendChild($domtree->createElement('VatCode', $product_detail['tax_rate']));
            $taxName = Product::getTaxesInformations($product_detail)['tax_name'];
            if(!empty($taxName))
                $product->appendChild($domtree->createElement('VatDescription', $taxName));
            if($pdcIsConfigured)
                $product->appendChild($domtree->createElement('IdPdc', $idPdc));
        }
        
        foreach ($order->getCartRules() as $cart_rule)
        {
            $this->trace('cart rule details', $cart_rule);
            
            $product = $products->appendChild($domtree->createElement('Row'));
            $code = $domtree->createElement('Code');
            $code->appendChild($domtree->createCDataSection($cart_rule['name']));
            $product->appendChild($code);
            $description = $domtree->createElement('Description');
            $description->appendChild($domtree->createCDataSection($cart_rule['name']));
            $product->appendChild($description);
            $product->appendChild($domtree->createElement('Qty', 1));
            $vat_code = round(($cart_rule['value']/$cart_rule['value_tax_excl'] - 1) * 100);
            $price_tax_excl = 100/(100+ $vat_code)*$cart_rule['value'];
            $product->appendChild($domtree->createElement('Price', - $price_tax_excl));
            $product->appendChild($domtree->createElement('VatCode', $vat_code));
            if($pdcIsConfigured)
                $product->appendChild($domtree->createElement('IdPdc', $idPdc));
        }
        
        if ($order->total_shipping_tax_excl > 0)
        {
            $shipping_company = Db::getInstance()->getRow("SELECT `name` FROM `" . _DB_PREFIX_ . "carrier` WHERE `id_carrier` = '" . pSQL($order->id_carrier) . "'");
            $product = $products->appendChild($domtree->createElement('Row'));
            $description = $domtree->createElement('Description');
            $description->appendChild($domtree->createCDataSection($this->l('Costi di spedizione:') . ' ' . $shipping_company['name']));
            $product->appendChild($description);
            $product->appendChild($domtree->createElement('Qty', '1'));
            $product->appendChild($domtree->createElement('Price', $order->total_shipping_tax_excl));
            $product->appendChild($domtree->createElement('VatCode', $order->carrier_tax_rate));
            if($pdcIsConfigured)
                $product->appendChild($domtree->createElement('IdPdc', $idPdc));
        }

        if ($isInvoice)
        {
            $fattura_invoice->appendChild($domtree->createElement('FootNotes', 'Rif. ordine: ' . $order->reference));
            $fattura_invoice->appendChild($domtree->createElement('Object', 'Ordine Prestashop N. ' . $order->id));
            $documentType = Configuration::get('PS_DISABILITA_RICEVUTE') == 1 ? 'I-force' : 'I';
            if($documentType == 'I' && empty($ga->vat_number)) // receipt
                $idNumerator = Configuration::get('PS_F24_SEZIONALE_RICEVUTA');
            else // invoice
                $idNumerator = Configuration::get('PS_F24_SEZIONALE_FATTURA');
            if($idNumerator != 'Predefinito')
                $fattura_invoice->appendChild($domtree->createElement('IdNumerator', $idNumerator));
            $query = 'SELECT docIdOrderFattura24 FROM '._DB_PREFIX_.'orders WHERE id_order=\'' . $order_id . '\';';
            $docIdOrderFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdOrderFattura24'];
            if($docIdOrderFattura24 != null)
                $fattura_invoice->appendChild($domtree->createElement('F24OrderId', $docIdOrderFattura24));
            $sendEmail = Configuration::get('PS_EMAIL_FATTURA') == 1 ? 'true' : 'false';
            $updateStorage = Configuration::get('PS_F24_MAGAZZINO_FATTURA');
            if($order->isVirtual())
                $idTemplate = Configuration::get('PS_F24_MODELLO_FATTURA');
            else
                $idTemplate = Configuration::get('PS_F24_MODELLO_FATTURA_DEST');
        }
        else // order
        {
            $fattura_invoice->appendChild($domtree->createElement('Number', $order->id));
            $documentType = 'C';
            $sendEmail = Configuration::get('PS_MAIL_ORDINE') == 1 ? 'true' : 'false';
            $updateStorage = Configuration::get('PS_F24_MAGAZZINO_ORDINE');
            if($order->isVirtual())
                $idTemplate = Configuration::get('PS_F24_MODELLO_ORDINE');
            else
                $idTemplate = Configuration::get('PS_F24_MODELLO_ORDINE_DEST');
        }
        $fattura_invoice->appendChild($domtree->createElement('DocumentType', $documentType));
        $fattura_invoice->appendChild($domtree->createElement('SendEmail', $sendEmail));
        if(!empty($updateStorage))
            $fattura_invoice->appendChild($domtree->createElement('UpdateStorage', $updateStorage));
        if(!empty($idTemplate) && $idTemplate !== 'Predefinito')
            $fattura_invoice->appendChild($domtree->createElement('IdTemplate', $idTemplate));
        
        return $domtree->saveXML();
    }

    public function downloadDocument($docIdFattura24, $orderId, $isInvoice)
    {
        $fattura24_api_url = 'https://www.app.fattura24.com/api/v0.3/GetFile';
        $send_data = array();
        $send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
        $send_data['docId'] = $docIdFattura24;
        $dataReturned = $this->curlDownload($fattura24_api_url, http_build_query($send_data));
        libxml_use_internal_errors(true);
        if (!simplexml_load_string(utf8_encode($dataReturned)))
        {
            if($isInvoice)
            {
                $order = new Order((int)$orderId);
                $fileName = $this->constructFileName($order->invoice_number,$order->date_add);
            }
            else
                $fileName = 'ORD'. sprintf('%06d', $orderId) . '.pdf';
            file_put_contents(_PS_DOWNLOAD_DIR_ . 'Fattura24/' . $fileName, $dataReturned);
        }
    }

    public function saveCustomer($check_order)
    {
    	$fattura24_api_url = 'https://www.app.fattura24.com/api/v0.3/SaveCustomer';
    	$send_data = array();
    	$send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
    	$send_data['xml'] = $this->createCustomerXml($check_order->id);
    	$dataReturned = $this->curlDownload($fattura24_api_url, http_build_query($send_data));
    }
    
    /* 
     * Funzione comune da eseguire dopo gli hook dichiarati sopra,
     * il parametro isInvoice serve per distinguere se deve essere creato un ordine o una fattura
     */
    public function afterHook($order, $isInvoice)
    {
        $orderId = $order->id;
        $fattura24_api_url = 'https://www.app.fattura24.com/api/v0.3/SaveDocument';
        $send_data = array();
        $send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
        $send_data['xml'] = $this->createInvoiceXml($orderId, $isInvoice);
        $dataReturned = $this->curlDownload($fattura24_api_url, http_build_query($send_data));
        $docIdFattura24 = explode('</docId>', explode('<docId>', $dataReturned)[1])[0];

        if(!empty($docIdFattura24))
        {
            if (!$isInvoice) // order
            {
                $query = 'UPDATE ' . _DB_PREFIX_ . 'orders SET docIdOrderFattura24=\'' . $docIdFattura24 . '\' WHERE id_order=\'' . $orderId . '\';';
                Db::getInstance()->Execute($query);
                $this->downloadDocument($docIdFattura24, $orderId, false);
            }
            else // invoice
            {
                $query = 'UPDATE ' . _DB_PREFIX_ . 'orders SET docIdFattura24=\'' . $docIdFattura24 . '\' WHERE id_order=\'' . $orderId . '\';';
                Db::getInstance()->Execute($query);
                $this->downloadDocument($docIdFattura24, $orderId, true); 
            }
        }

        $response= new DOMDocument();
        $response->preserveWhiteSpace = false;
        $response->formatOutput = true;
        $response->loadXML($dataReturned);
        $response_string = $response->saveXML();

        $this->trace($send_data['xml'], $response_string);
    }

    public function testApiKey()
    {
        $fattura24_api_url = 'https://www.app.fattura24.com/api/v0.3/TestKey';
        $send_data = array();
        $send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
        $dataReturned = $this->curlDownload($fattura24_api_url, http_build_query($send_data));
        $xml = simplexml_load_string(str_replace('&egrave;', 'è', $dataReturned));
        $subscriptionTypeIsValid = true;
        $subscriptionDaysToExpiration = 365;
        if(is_object($xml))
        {
            $returnCode = intval($xml->returnCode);
            $description = strval($xml->description);
            if($returnCode == 1)
            {
                $subscriptionType = intval($xml->subscription->type);
                if($subscriptionType != 5 && $subscriptionType != 6)
                    $subscriptionTypeIsValid = false;
                $subscriptionExpire = strval($xml->subscription->expire);
                $date1 = $this->now();
                $date2 = str_replace('/', '-', $subscriptionExpire);
                $diff = abs(strtotime($date1) - strtotime($date2));
                $subscriptionDaysToExpiration = ceil($diff / 86400);                
            }
        }
        else
        {
            $returnCode = '?';
            $description = 'Errore generico, per favore contatta il nostro servizio tecnico a info@fattura24.com';
        }
        return array(
            'returnCode' => $returnCode,
            'description' => $description,
            'subscriptionTypeIsValid' => $subscriptionTypeIsValid,
            'subscriptionDaysToExpiration' =>  $subscriptionDaysToExpiration
        );
    }

    /*
     * Get Template List
     */
    public function getTemplate($isOrder)
    {
        $fattura24_api_url = 'https://www.app.fattura24.com/api/v0.3/GetTemplate';
        $send_data = array();
    	$send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
        $dataReturned = $this->curlDownload($fattura24_api_url, http_build_query($send_data));
        $listaNomi = array();
        $listaNomi['Predefinito'] = 'Predefinito';
        $xml = simplexml_load_string(utf8_encode($dataReturned));
        if (is_object($xml))
        {
            $listaModelli = $isOrder ? $xml->modelloOrdine : $xml->modelloFattura;
            foreach($listaModelli as $modello)
                $listaNomi[intval($modello->id)] = strval($modello->descrizione);
        }
        else
            $this->trace('error list templates', $dataReturned);
        return $listaNomi;
    }
    
    /*
     * Get Pdc List
     */
    public function getPdc()
    {
        $fattura24_api_url = 'https://www.app.fattura24.com/api/v0.3/GetPdc';
        $send_data = array();
    	$send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
        $dataReturned = $this->curlDownload($fattura24_api_url, http_build_query($send_data));
        $listaNomi = array();
        $listaNomi['Nessun Pdc'] = 'Nessun Pdc';
        $xml = simplexml_load_string(utf8_encode($dataReturned));
        if (is_object($xml))
        {
            foreach($xml->pdc as $pdc)
                if(intval($pdc->ultimoLivello) == 1)
                    $listaNomi[intval($pdc->id)] = str_replace('^', '.', strval($pdc->codice)) . ' - ' . strval($pdc->descrizione);
        }
        else
            $this->trace('error list pdc', $dataReturned);
        return $listaNomi;
    }

    /*
    * Get Sezionale List
    */
    function GetNumerator($idTipoDocumento)
    {
        $fattura24_api_url = 'https://www.app.fattura24.com/api/v0.3/GetNumerator';
        $send_data = array();
    	$send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
        $dataReturned = $this->curlDownload($fattura24_api_url, http_build_query($send_data));
        $listaNomi = array();
        $listaNomi['Predefinito'] = 'Predefinito';
        $xml = simplexml_load_string(utf8_encode($dataReturned));
        if (is_object($xml))
        {
            foreach($xml->sezionale as $sezionale)
                foreach($sezionale->doc as $doc)
                    if(intval($doc->id) == $idTipoDocumento && intval($doc->stato) == 1)
                        $listaNomi[intval($sezionale->id)] = strval($sezionale->code);
        }
        else
            $this->trace('error list sezionale', $dataReturned);
        return $listaNomi;
    }

    public function curlDownload($url, $data_string)
    {
        if(!function_exists('curl_init'))
        {
            $this->trace('curl is not installed');
            die('Sorry, cURL is not installed!');
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        $config = array();
        $config['useragent'] = 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0';
        curl_setopt($ch, CURLOPT_USERAGENT, $config['useragent']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        
        if(!($output = curl_exec($ch)))
            $this->trace('curl error', curl_getinfo($ch), curl_error($ch), curl_errno($ch));
        curl_close($ch);
        return $output;
    }

    function now($fmt = 'Y-m-d H:i:s', $tz = 'Europe/Rome')
    {
        $timestamp = time();
        $dt = new \DateTime("now", new \DateTimeZone($tz));
        $dt->setTimestamp($timestamp);
        return $dt->format($fmt);
    }
    
    function checkNewVersion($urlCheckVersion)
    {
        try
        {
            $fileCheckVersion = fopen($urlCheckVersion, "r");
            if($fileCheckVersion)
            {
                $content = stream_get_contents($fileCheckVersion);
                fclose($fileCheckVersion);
                return $content;
            }
        }
        catch(Exception $e){}
        return false;
    }
    
    function getVersionCheckMessage()
    {
        $versionCheckMessage = '';
        $urlCheckVersion = "https://www.fattura24.com/prestashop/latest_version_pre_1.txt";
        $checkNewVersion = $this->checkNewVersion($urlCheckVersion);
        if($checkNewVersion)
        {
            $latest_version_date = substr($checkNewVersion,0,16);
            if($latest_version_date !== $this->moduleDate)
            {
                $urlDownload = substr($checkNewVersion,17);
                return 'È stata rilasciata una nuova versione del modulo! Clicca <a href="' . $urlDownload . '" target="_blank">qui</a> per scaricarla';
            }
        }
        
        return '';
    }
    
    function trace()
    {
        $fp = fopen(_PS_MODULE_DIR_. 'fattura24/log/trace.log' , "a+");
        fprintf($fp, "%s: %s\n\n", $this->now(), var_export(func_get_args(), true));
        fclose($fp);
    }

    // [30.08.2018]
    // Gaetano
    // Funzione di utilità per stampare variabili sul file di log del modulo, in assenza di un sistema di debug disponibile.
    function traceDebug($string){
        $fp = fopen(_PS_MODULE_DIR_. 'fattura24/log/traceDebug.log' , "a+");
        fprintf($fp, $string."\n");
        fclose($fp);
    }
    

    function getInfo()
    {
        return 'Versione del modulo di Fattura24: ' . $this->version . '\nVersione di Prestashop: ' 
            . _PS_VERSION_ . '\nVersione di PHP: ' . PHP_VERSION . '\nUrl negozio: ' . _PS_BASE_URL_;
    }
}
