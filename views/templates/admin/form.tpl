<style type="text/css"> 
   .checkbox {
        margin-left: 30px;
    }
    .help {
        margin-top: 9px;
    }
    .help > a {
        width: 15px;
        height: 15px;
        border-radius: 50%;
        background-color: #000000;
        color: #ffffff;
        padding: 0 5px 0 5px;
    }
    .help > a:focus {
        text-decoration: none;
    }
    .fattura24-field {
        margin-left: 30px;
    }
    .fattura24-label {
        font-size: 14px !important;
    }
    .fattura24-comment {
        font-size: 13px;
        font-style: italic;
        color: grey;
        padding-left: 0.5em;
    }
    .fattura24-label-correctApiKey {
        font-size: 13px !important;
        color: green;
        padding-left: 1.5em;
        margin-top: 0.5em;
    }
    .fattura24-label-fe-info {
        font-size: 10px !important;
        color: red;
        padding-left: 1.5em;
        margin-top: 0.5em;
    }
    .fattura24-error {
        padding: 5px;
        padding-left: 10px;
        padding-right: 10px;
        border-radius: 5px;
        color: #fff;
        background-color: #ff6000;
    }
    .fattura24-warning {
        padding: 5px;
        padding-left: 10px;
        padding-right: 10px;
        border-radius: 5px;
        color: #fff;
        background-color: #ffbf00;
    }
</style>

<form id="module_form" class="defaultForm form-horizontal"
      action="index.php?controller=AdminModules&configure=fattura24&tab_module=administration&module_name=fattura24&token={$token}"
      method="post" enctype="multipart/form-data" novalidate>
    <input type="hidden" name="submitModule" value="1"/>

    <div class="panel" id="fieldset_0">
        <div class="panel-heading">
            <i class="icon-cogs"></i> Fattura24
        </div>

        <button type="submit" value="1" id="module_form_submit_btn_header" name="submitModule"
            class="btn btn-default pull-right" style="margin-bottom:10px">
            <i class="process-icon-save"></i> Salva
        </button>

        {if !$isFattura24Enabled}
            <div class="form-group" style="margin-bottom:10px">
                <div class="col-lg-12">
                    <div class ="fattura24-error">
                        <h4>MODULO NON ATTIVATO</h4>
                    </div>
                </div>
            </div>
        {/if}

        {if $psDisableOverrides == 1}
            <div class="form-group" style="margin-bottom:10px">
                <div class="col-lg-12">
                    <div class ="fattura24-error">
                        <h4>ATTENZIONE CONFIGURAZIONE ERRATA:</h4>
                        <h4>Per visualizzare i PDF delle fatture, devi impostare su 'No' la seguente impostazione: 
                        Parametri avanzati -> Prestazioni -> Disattiva tutti gli override</h4>
                    </div>
                </div>
            </div>
        {/if}

        {if $versionCheckMessage != ''}
            <div class="form-group" style="margin-bottom:10px">
                <div class="col-lg-12">
                    <div class ="fattura24-warning">
                        <h4>{$versionCheckMessage}</h4>
                    </div>
                </div>
            </div>
        {/if}

        {if $apiKeyCheck.returnCode != 1}
            <div class="form-group" style="margin-bottom:10px">
                <div class="col-lg-12">
                    <div class ="fattura24-error">
                        {if !empty($apiKey)}
                            <h4>API KEY NON CORRETTA:</h4>
                            <h4>{$apiKeyCheck.description}</h4>
                        {else}
                            <h4>API KEY NON INSERITA</h4>
                        {/if}
                    </div>
                </div>
            </div>
        {/if}

        {if !$apiKeyCheck.subscriptionTypeIsValid}
            <div class="form-group" style="margin-bottom:10px">
                <div class="col-lg-12">
                    <div class ="fattura24-error">
                        <h4>ATTENZIONE:</h4>
                        <h4>Per utilizzare questo servizio devi avere un abbonamento Business</h4>
                    </div>
                </div>
            </div>
        {/if}

        {if $apiKeyCheck.subscriptionDaysToExpiration < 31}
            <div class="form-group" style="margin-bottom:10px">
                <div class="col-lg-12">
                    <div class ="fattura24-warning">
                        <h4>Mancano {$apiKeyCheck.subscriptionDaysToExpiration} giorni alla scadenza del tuo abbonamento</h4>
                    </div>
                </div>
            </div>
        {/if}

        <div class="form-wrapper">
            <div class="form-group col-lg-12" style="margin-top:20px">
                <h3>IMPOSTAZIONI GENERALI</h3>                
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">Versione modulo</label>
                <div class="col-lg-1">
                    <div class="fattura24-field">
                        <h4 id='version'>{$version}</h4>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Api Key
                </label>
                <div class="col-lg-4">
                    <div class="fattura24-field">
                        <input type="text"
                            name="PS_FATTURA24_API"
                            id="PS_FATTURA24_API"
                            value="{$apiKey}"
                        />
                    </div>
                </div>

                <div class="col-lg-4">
                    {if $apiKeyCheck.returnCode == 1}
                        <label class="fattura24-label-correctApiKey">
                            Api Key verificata
                        </label>
                    {/if}
                </div>
            </div>

            <br/>
            <div class="form-group col-lg-12">
                <h3>Rubrica</h3>
            </div>
            
            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Salva cliente
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo per salvare i dati del cliente nella rubrica di Fattura24 
                            quando viene creato un ordine in Prestashop o quando crei un documento (ordine o fattura) in Fattura24">?</a>
                    </span>
                </label>
                <div class="col-lg-1">
                    <div class="checkbox">
                        <label for="PS_SALVA_CLIENTE_">
                            <input type="checkbox" name="PS_SALVA_CLIENTE_"
                                   id="PS_SALVA_CLIENTE_" class="" value="1"
                                    {if $salvaCliente eq 1}
                                        checked="checked"
                                    {/if}
                            />
                        </label>
                    </div>
                </div>
            </div>
            
            <br/>
            <div class="form-group col-lg-12">
                <h3>Ordini</h3>
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Crea ordine&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi attivare la creazione dell'ordine in Fattura24">?</a>
                    </span>
                </label>
                <div class="col-lg-1">
                    <div class="checkbox">
                        <label for="PS_CREAZIONE_ORDINE_">
                            <input type="checkbox" name="PS_CREAZIONE_ORDINE_"
                                   id="PS_CREAZIONE_ORDINE_" class="" value="1"
                                    {if $creazioneOrdine eq 1}
                                        checked="checked"
                                    {/if}
                            />
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Scarica PDF&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi che venga scaricato automaticamente in locale il PDF dell'ordine">?</a>
                    </span>
                </label>
                <div class="col-lg-1">
                    <div class="checkbox">
                        <label for="PS_PDF_ORDINE_">
                            <input type="checkbox" name="PS_PDF_ORDINE_"
                                   id="PS_PDF_ORDINE_"
                                   value="1"
                                    {if $pdfOrdine eq 1}
                                        checked="checked"
                                    {/if}
                            />
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Invia email&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi che il PDF dell'ordine venga spedito automaticamente al cliente via email">?</a>
                    </span>
                </label>
                <div class="col-lg-1">
                    <div class="checkbox">
                        <label for="PS_MAIL_ORDINE_">
                            <input type="checkbox" name="PS_MAIL_ORDINE_"
                                   id="PS_MAIL_ORDINE_"
                                   value="1"
                                    {if $emailOrdine eq 1}
                                        checked="checked"
                                    {/if}
                            />
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Movimenta magazzino&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi attivare la movimentazione del magazzino tramite la creazione degli ordini. 
                            I prodotti in Fattura24 che hanno lo stesso codice dei prodotti di Prestashop saranno impegnati">?</a>
                    </span>
                </label>
                <div class="col-lg-1">
                    <div class="checkbox">
                        <label for="PS_F24_MAGAZZINO_ORDINE_">
                            <input type="checkbox" name="PS_F24_MAGAZZINO_ORDINE_"
                                   id="PS_F24_MAGAZZINO_ORDINE_" class="" value="1"
                                    {if $magazzinoOrdine eq 1}
                                        checked="checked"
                                    {/if}
                            />
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Modello ordine&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Seleziona il modello da usare per la creazione del PDF dell'ordine tra i tuoi modelli ordine in Fattura24. 
                            Questo modello verrà usato se nell'ordine non è presente una destinazione.
                            Se è selezionato 'Predefinito', sarà usato il modello impostato come modello predefinito in Fattura24. 
                            Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                    </span>
                </label>
                <div class="col-lg-4">
                    <div class="fattura24-field">
                        <select name="PS_F24_MODELLO_ORDINE_" id="PS_F24_MODELLO_ORDINE_">
                            {html_options options=$listaModelliOrdine selected=$modelloOrdine}
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Modello ordine accompagnatorio&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Seleziona il modello da usare per la creazione del PDF dell'ordine tra i tuoi modelli ordine in Fattura24. 
                            Questo modello verrà usato se nell'ordine è presente una destinazione.
                            Se è selezionato 'Predefinito', sarà usato il modello impostato come modello predefinito in Fattura24. 
                            Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                    </span>
                </label>
                <div class="col-lg-4">
                    <div class="fattura24-field">
                        <select name="PS_F24_MODELLO_ORDINE_DEST_" id="PS_F24_MODELLO_ORDINE_DEST_">
                            {html_options options=$listaModelliOrdine selected=$modelloOrdineDest}
                        </select>
                    </div>
                </div>
            </div>

            <br/>
            <div class="form-group col-lg-12">
                <h3>Fatture</h3>
            </div>

        <!--
           * [19.10.2018]
           * Gaetano
           * Modifica campo crea fattura per la gestione della fatturazione elettronica
         -->
            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Crea fattura&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi che venga creata la fattura tramite Fattura24 e che sia scaricata sul tuo Prestashop quando lo stato 
                            dell'ordine viene impostato come 'pagato">?</a>
                    </span>
                </label>
                <div class="col-lg-4">
                    <div class="fattura24-field">
                        <select name="PS_CREAZIONE_FATTURA_" id="PS_CREAZIONE_FATTURA_">
                            {html_options options=$opzioniCreaFattura selected=$creazioneFattura}
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <label class="fattura24-label-fe-info" name="labelFeInfo" id="labelFeInfo">
                        Attenzione: per i clienti stranieri verrà prodotta una fattura tradizionale NON elettronica in formato PDF
                    </label>
                </div>
            </div>


            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Invia email&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi che il PDF della fattura venga spedito automaticamente al cliente via email">?</a>
                    </span>
                </label>
                <div class="col-lg-1">
                    <div class="checkbox">
                        <label for="PS_EMAIL_FATTURA_">
                            <input type="checkbox" name="PS_EMAIL_FATTURA_"
                                   id="PS_EMAIL_FATTURA_"
                                   value="1"
                                    {if $emailFattura eq 1}
                                        checked="checked"
                                    {/if}
                            />
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Movimenta magazzino&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi attivare la movimentazione del magazzino tramite la creazione delle fatture. 
                            I prodotti in Fattura24 che hanno lo stesso codice dei prodotti di Prestashop saranno scaricati">?</a>
                    </span>
                </label>
                <div class="col-lg-1">
                    <div class="checkbox">
                        <label for="PS_F24_MAGAZZINO_FATTURA_">
                            <input type="checkbox" name="PS_F24_MAGAZZINO_FATTURA_"
                                   id="PS_F24_MAGAZZINO_FATTURA_"
                                   value="1"
                                    {if $magazzinoFattura eq 1}
                                        checked="checked"
                                    {/if}
                            />
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Stato 'Pagata'&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi che la fattura venga creata direttamente come 'Pagata'">?</a>
                    </span>
                </label>
                <div class="col-lg-1">
                    <div class="checkbox">
                        <label for="PS_STATO_PAGATO_">
                            <input type="checkbox" name="PS_STATO_PAGATO_"
                                   id="PS_STATO_PAGATO_"
                                   value="1"
                                    {if $statoPagato eq 1}
                                        checked="checked"
                                    {/if}
                            />
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Disabilita creazione ricevute&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi che la fattura sia creata anche in assenza della Partita IVA del cliente">?</a>
                    </span>
                </label>
                <div class="col-lg-1">
                    <div class="checkbox">
                        <label for="PS_DISABILITA_RICEVUTE_">
                            <input type="checkbox" name="PS_DISABILITA_RICEVUTE_"
                                   id="PS_DISABILITA_RICEVUTE_"
                                   value="1"
                                    {if $disabilitaRicevute eq 1}
                                        checked="checked"
                                    {/if}
                            />
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Modello fattura&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Seleziona il modello da usare per la creazione del PDF della fattura tra i tuoi modelli fattura in Fattura24. 
                            Questo modello verrà usato se nell'ordine non è presente una destinazione.
                            Se è selezionato 'Predefinito', sarà usato il modello impostato come modello predefinito in Fattura24. 
                            Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                    </span>
                </label>
                <div class="col-lg-4">
                    <div class="fattura24-field">
                        <select name="PS_F24_MODELLO_FATTURA_" id="PS_F24_MODELLO_FATTURA_">
                            {html_options options=$listaModelliFattura selected=$modelloFattura}
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Modello fattura accompagnatoria&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Seleziona il modello da usare per la creazione del PDF della fattura tra i tuoi modelli fattura in Fattura24. 
                            Questo modello verrà usato se nell'ordine è presente una destinazione.
                            Se è selezionato 'Predefinito', sarà usato il modello impostato come modello predefinito in Fattura24. 
                            Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                    </span>
                </label>
                <div class="col-lg-4">
                    <div class="fattura24-field">
                        <select name="PS_F24_MODELLO_FATTURA_DEST_" id="PS_F24_MODELLO_FATTURA_DEST_">
                            {html_options options=$listaModelliFattura selected=$modelloFatturaDest}
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Piano dei conti&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Puoi selezionare il conto da associare alle prestazioni/prodotti dei documenti tra i tuoi conti in Fattura24. 
                            Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                    </span>
                </label>
                <div class="col-lg-4">
                    <div class="fattura24-field">
                        <select name="PS_F24_PDC_" id="PS_F24_PDC_">
                            {html_options options=$listaConti selected=$conto}
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Sezionale ricevute&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Puoi selezionare il sezionale da usare per la numerazione delle ricevute tra i sezionali attivi per le ricevute in Fattura24.
                            Se è selezionato 'Predefinito', sarà usato il sezionale che hai impostato come predefinito in Fattura24.
                            Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                    </span>
                </label>
                <div class="col-lg-4">
                    <div class="fattura24-field">
                        <select name="PS_F24_SEZIONALE_RICEVUTA_" id="PS_F24_SEZIONALE_RICEVUTA_">
                            {html_options options=$listaSezionaliRicevuta selected=$sezionaleRicevuta}
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="fattura24-label control-label col-lg-3">
                    Sezionale fatture&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Seleziona il sezionale da usare per la numerazione delle fatture tra i sezionali attivi per le fatture in Fattura24.
                            Se è selezionato 'Predefinito', sarà usato il sezionale che hai impostato come predefinito in Fattura24.
                            Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                    </span>
                </label>
                <div class="col-lg-4">
                    <div class="fattura24-field">
                        <select name="PS_F24_SEZIONALE_FATTURA_" id="PS_F24_SEZIONALE_FATTURA_">
                            {html_options options=$listaSezionaliFattura selected=$sezionaleFattura}
                        </select>
                    </div>
                </div>
            </div>

            <br/>
            <div class="form-group col-lg-12">
                <h3>Log</h3>
            </div>

            <div class="form-group">
                <div class="col-lg-2">                        
                    <input type="button" class="btn btn-default" id="download_log_file" value="Scarica file di log"/>&nbsp;&nbsp;&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Scarica e invia questo file a info@fattura24.com quando riscontri problemi con il modulo di Fattura24">?</a>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-3">                     
                    <input type="button" class="btn btn-default" id="delete_log_file" value="Cancella file di log"/>&nbsp;
                    <span class="help">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Cancella il file di log dalla cartella del modulo di Fattura24. Ciò è utile per liberare spazio sul proprio server">?</a>
                    </span>
                </div>
            </div>
        </div><!-- /.form-wrapper -->

        <div class="panel-footer">
            <button type="submit" value="1" id="module_form_submit_btn_footer" name="submitModule"
                    class="btn btn-default pull-right">
                <i class="process-icon-save"></i> Salva
            </button>
        </div>
    </div>
</form>

<script type="text/javascript">

    console.log('eccomi');
    
    function downloadFile(dataurl, filename)
    {
        var a = document.createElement("a");
        a.href = dataurl;
        a.setAttribute("download", filename);
        var b = document.createEvent("MouseEvents");
        b.initEvent("click", false, true);
        a.dispatchEvent(b);
        return false;
    }

    document.addEventListener("DOMContentLoaded", function (e)
    {
        var downloadLogFile = document.getElementById("download_log_file");
        downloadLogFile.addEventListener('click', function (e)
        {
            $.ajax({
                method: 'POST',
                data: 'info={$info}',
                url: '{$moduleUrl}' + 'log/downloadLogFile.php',
                dataType: 'json',
                error: function (response)
                {
                    alert('Errore durante il download.');
                },
                complete: function()
                {
                    downloadFile('{$moduleUrl}' + 'log/trace.log', 'trace.log');
                }
            });
        });

        var deleteLogFile = document.getElementById("delete_log_file");
        deleteLogFile.addEventListener('click', function (e)
        {
            var r = confirm("Sei sicuro di voler cancellare il file di log di Fattura24?");
            if (r == true)
            {
                $.ajax({
                    url: '{$moduleUrl}' + 'log/deleteLogFile.php',
                    dataType: 'json',
                    success: function (response)
                    {
                        if(response.fileExists === true)
                        {
                            alert('File cancellato! La sua dimensione era: ' + response.size + ' KB.');
                        }
                        else if(response.fileExists === false)
                        {
                            alert('Il file è già stato cancellato.');
                        }
                    }
                });
            }
        });

        // Rubrica
        var PS_SALVA_CLIENTE = document.getElementById('PS_SALVA_CLIENTE_');

        // Ordini
        var PS_CREAZIONE_ORDINE = document.getElementById('PS_CREAZIONE_ORDINE_');
        var PS_PDF_ORDINE = document.getElementById('PS_PDF_ORDINE_');
        var PS_MAIL_ORDINE = document.getElementById('PS_MAIL_ORDINE_');
        var PS_F24_MAGAZZINO_ORDINE = document.getElementById('PS_F24_MAGAZZINO_ORDINE_');
        var PS_F24_MODELLO_ORDINE = document.getElementById('PS_F24_MODELLO_ORDINE_');
        var PS_F24_MODELLO_ORDINE_DEST = document.getElementById('PS_F24_MODELLO_ORDINE_DEST_');

        if (!PS_CREAZIONE_ORDINE.checked){
            disable(PS_PDF_ORDINE);
            disable(PS_MAIL_ORDINE);
            disable(PS_F24_MAGAZZINO_ORDINE);
            disable(PS_F24_MODELLO_ORDINE);
            disable(PS_F24_MODELLO_ORDINE_DEST);
        }
        if (PS_CREAZIONE_ORDINE.checked){
            PS_SALVA_CLIENTE.checked = true;
            PS_SALVA_CLIENTE.disabled = true;
        }
        // Listener per creazione ordine
        PS_CREAZIONE_ORDINE.addEventListener('change', function (e){
            if (PS_CREAZIONE_ORDINE.checked){
                enable(PS_PDF_ORDINE);
                enable(PS_MAIL_ORDINE);
                enable(PS_F24_MAGAZZINO_ORDINE);
                enable(PS_F24_MODELLO_ORDINE);
                enable(PS_F24_MODELLO_ORDINE_DEST);
                PS_SALVA_CLIENTE.checked = true;
                PS_SALVA_CLIENTE.disabled = true;
            }
            else{
                disable(PS_PDF_ORDINE);
                disable(PS_MAIL_ORDINE);
                disable(PS_F24_MAGAZZINO_ORDINE);
                disable(PS_F24_MODELLO_ORDINE);
                disable(PS_F24_MODELLO_ORDINE_DEST);
                if(!PS_CREAZIONE_FATTURA.checked)
                    PS_SALVA_CLIENTE.disabled = false;
            }
        });

        /* [19.10.2018]
        *  Gaetano
        *  Modificata voce ui crea fattura in una select per permettere la gestione della fatturazione elettronica
        */
        var PS_CREAZIONE_FATTURA = document.getElementById('PS_CREAZIONE_FATTURA_');
        var valueSelected = PS_CREAZIONE_FATTURA.options[PS_CREAZIONE_FATTURA.selectedIndex].text;
        console.log("VALUE PS_CREAZIONE_FATTURA = "+valueSelected);
        var PS_DISABILITA_RICEVUTE = document.getElementById('PS_DISABILITA_RICEVUTE_');
        var PS_EMAIL_FATTURA = document.getElementById('PS_EMAIL_FATTURA_');
        var PS_STATO_PAGATO = document.getElementById('PS_STATO_PAGATO_');
        var PS_F24_MAGAZZINO_FATTURA = document.getElementById('PS_F24_MAGAZZINO_FATTURA_');
        var PS_F24_MODELLO_FATTURA = document.getElementById('PS_F24_MODELLO_FATTURA_');
        var PS_F24_MODELLO_FATTURA_DEST = document.getElementById('PS_F24_MODELLO_FATTURA_DEST_');
        var PS_F24_PDC = document.getElementById('PS_F24_PDC_');
        var PS_F24_SEZIONALE_RICEVUTA = document.getElementById('PS_F24_SEZIONALE_RICEVUTA_');
        var PS_F24_SEZIONALE_FATTURA = document.getElementById('PS_F24_SEZIONALE_FATTURA_');
        var labelFeInfo = document.getElementById('labelFeInfo');
        
        if (valueSelected === "Disattivata"){
            disable(PS_DISABILITA_RICEVUTE);
            disable(PS_EMAIL_FATTURA);
            disable(PS_STATO_PAGATO);
            disable(PS_F24_MAGAZZINO_FATTURA);
            disable(PS_F24_MODELLO_FATTURA);
            disable(PS_F24_MODELLO_FATTURA_DEST);
            disable(PS_F24_PDC);
            disable(PS_F24_SEZIONALE_RICEVUTA);
            disable(PS_F24_SEZIONALE_FATTURA);
            labelFeInfo.style.display = 'none';
        }

        if (valueSelected !== "Disattivata"){
            PS_SALVA_CLIENTE.checked = true;
            PS_SALVA_CLIENTE.disabled = true;
            labelFeInfo.style.display = 'none';
            if(valueSelected === "Fattura elettronica"){
                disable(PS_EMAIL_FATTURA);
                labelFeInfo.style.display = 'block';
            }
        }

        // Listener per creazione fatture
        PS_CREAZIONE_FATTURA.addEventListener('change', function (e){
            var valueSelected = PS_CREAZIONE_FATTURA.options[PS_CREAZIONE_FATTURA.selectedIndex].text;
            console.log("SELECTED OPTION FATTURA = "+valueSelected);
            if (valueSelected !== "Disattivata"){
                enable(PS_EMAIL_FATTURA);
                enable(PS_STATO_PAGATO);
                enable(PS_DISABILITA_RICEVUTE);
                enable(PS_F24_MAGAZZINO_FATTURA);
                enable(PS_F24_MODELLO_FATTURA);
                enable(PS_F24_MODELLO_FATTURA_DEST);
                enable(PS_F24_PDC);
                enable(PS_F24_SEZIONALE_RICEVUTA);
                enable(PS_F24_SEZIONALE_FATTURA);
                labelFeInfo.style.display = 'none';
                if(valueSelected === "Fattura elettronica"){
                    disable(PS_EMAIL_FATTURA);
                    labelFeInfo.style.display = 'block';
                }
                PS_SALVA_CLIENTE.checked = true;
                PS_SALVA_CLIENTE.disabled = true;
            }
            else if(valueSelected === "Disattivata"){
                disable(PS_EMAIL_FATTURA);
                disable(PS_STATO_PAGATO);
                disable(PS_DISABILITA_RICEVUTE);
                disable(PS_F24_MAGAZZINO_FATTURA);
                disable(PS_F24_MODELLO_FATTURA);
                disable(PS_F24_MODELLO_FATTURA_DEST);
                disable(PS_F24_PDC);
                disable(PS_F24_SEZIONALE_RICEVUTA);
                disable(PS_F24_SEZIONALE_FATTURA);
                labelFeInfo.style.display = 'none';
                if(!PS_CREAZIONE_ORDINE.checked)
                    PS_SALVA_CLIENTE.disabled = false;
            }
        });

        function disable(element){
            element.checked = false;
            element.disabled = true;
        }

        function enable(element){
            element.disabled = false;
        }

        //GESTIONE DEI TOOLTIP
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>