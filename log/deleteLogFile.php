<?php

$response = array('status'=>false);
$filePath = dirname(__FILE__) . '/trace.log';

if(file_exists($filePath))
{
    $response['fileExists'] = true;
    $response['size'] = round(filesize($filePath) / 1000.);
    unlink($filePath);
}
else
{
    $response['fileExists'] = false;
}

$response['path'] = $filePath;
$response['status'] = true;
echo json_encode($response);